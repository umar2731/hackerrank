from pprint import pprint
import itertools
from OpenTestCase import *
topic = ['10101', '11100', '11010', '00101']
topic = ['11101', '10101', '11001', '10111', '10000', '01110']
topic = openTestCaseWOspace()

def hitung( x, y ) :
  c = 0
  r = [0] * len(x)
  for i in range ( len(x)) :
    if x[i] + y[i] == 0 :
      r[i] = 0
    else :
      r[i] = 1
      c += 1
    #x[i] = x[i] + y[i]
  return r, c  # r adalah array hasil hitung, c adalah jumlah penguasaan tim terhadap topic

def acmTeam( topic ):
  # masukkan input topic ke dalam array of int na
  na = []
  for a in topic:
    ai = []
    for b in a:
#      print(  b, end="" )
      ai.append( int( b )) 
    na.append( ai )
#    print("")
 # pprint( na )

#  print( hitung( na[0], na[1] ))
  ans = 0
  mt = 0 # max topic
  ak = []  # array of k
  for i in range( len(topic)) :
    for j in range(i+1,len(topic)) :
#      print( i, j)
       r, k = hitung( na[i], na[j] )
       ak.append( k )

       if( k > mt ) :
         mt = k
  print( mt )

  for k in ak :
    if k == mt :
      ans += 1 
# Looping bertingkat 2 berikut ini mengakibatkan timeout di server, karena
# perhitungan ini sudah dilakukan diatas, tidak perlu dilakukan lagi      
#   for i in range( len(topic)) :
#     for j in range(i+1,len(topic)) :
# #      print( i, j)
#        r, k = hitung( na[i], na[j] )
#        if( k == mt ) :
#          ans += 1
  
  print(mt, ans )
  return mt, ans

acmTeam(topic)

def acmTeam_1a(topics):
  na = [[0]*len(topic[0])]*len(topic)
  i = 0
  j = 0
  pprint( na )
  print( "==")
  na[0][0] = 7
  pprint( na )
  print( "==")
  for a in topic:
    j = 0
    for b in a:
      print( i ,j, " " , b ) 
      na[i][j] = int(b)
      pprint( na )
      print( "")
      j += 1
    i += 1
  pprint(na)




from array import *
def array_concept() :
  #T = [[11, 12, 5, 2], [15, 6,10,2], [10, 8, 12, 5], [12,15,8,6]]
  T = [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0,0]]
  #T[2] = [11,9]
  T[0][3] = 7
  T[1][1] = 7
  
  # for r in T:
  #     for c in r:
  #         print(c,end = " ")
  #     print()
  pprint( T )