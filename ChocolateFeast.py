n = 12  # money
c = 4 # cost of chocolate bar
m = 4

# output : 6
def chocolateFeast1(n, c, m):
  bonus = 0
  get_chocolate = n // c
  if get_chocolate < m:
    print(get_chocolate)
    bonus += 1
  print(bonus + get_chocolate)

def chocolateFeast(cash, cost, wrapper_cost):
    chocolate = cash//cost
    wrappers = chocolate

    while wrappers // wrapper_cost > 0:
        new_chocolate = wrappers // wrapper_cost
        wrappers -= new_chocolate * wrapper_cost
        wrappers += new_chocolate
        chocolate += new_chocolate
        
    print(chocolate)

chocolateFeast(n, c, m)